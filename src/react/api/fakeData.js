const data = [
    {
        id: 1,
        title: "Нямушка",
        subtitle: "с фуа-гра",
        promo: "Сказочное заморское яство",
        promoHovered: "Котэ не одобряет?",
        cardDescHovered: "Печень утки разварная с артишоками.",
        description: ["10 порций", "мышь в подарок"],
        inStock: true,
        weight: "0,5",
        weightText: "кг",
        icon: "./img/cat.png"
    },
    {
        id: 2,
        title: "Нямушка",
        subtitle: "с рыбой",
        promo: "Сказочное заморское яство",
        promoHovered: "Котэ не одобрит.",
        cardDescHovered: "Мяско рыбки.",
        description: ["40 порций", "2 мыши в подарок"],
        inStock: true,
        weight: "5",
        weightText: "кг",
        icon: "./img/cat.png"
    },
    {
        id: 3,
        title: "Нямушка",
        subtitle: "с курой",
        promo: "Сказочное заморское яство",
        promoHovered: "Котэ одобряет же.",
        cardDescHovered: "Печень утки разварная с артишоками.",
        description: ["100 порций", "5 мышей в подарок", "заказчик доволен"],
        inStock: false,
        weight: "5",
        weightText: "кг",
        icon: "./img/cat.png"
    },
];

export default data;
