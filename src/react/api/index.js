import data from "./fakeData";

export function getCardsProduct() {
    return new Promise(resolve => {
        setTimeout(()=> {
            resolve({
                success: true,
                data
            });
        }, 0);
    });
}
