
import React from "react";
import ReactDOM from "react-dom";

import CardList from "./modules/card-list/index.jsx";

import {compose, createStore, applyMiddleware} from "redux";
import rootReducer from "./store/reducers/rootReducer";
import {Provider} from "react-redux";
import thunk from "redux-thunk";

let store;

if(navigator.userAgent.indexOf("MSIE")!==-1
    || navigator.appVersion.indexOf("Trident/") > -1){
    /* Microsoft Internet Explorer detected in. */
    store = createStore(rootReducer, compose(
        applyMiddleware(
            thunk,
        ),
    ));
} else {
    store = createStore(rootReducer, compose(
        applyMiddleware(
            thunk,
        ),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    ));
}



const App = () => {
    return (<React.Fragment>
        <CardList/>
    </React.Fragment>);
};

const renderDom = document.getElementById("root");
if (renderDom !== null) {
    ReactDOM.render(<Provider store={store}><App/></Provider>, renderDom);
}
