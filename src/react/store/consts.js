export const FETCH_CARDS = "FETCH_CARDS";
export const SHOW_LOADER = "SHOW_LOADER";
export const HIDE_LOADER = "HIDE_LOADER";
export const SELECT_CARD = "SELECT_CARD";
export const UNSELECT_CARD = "UNSELECT_CARD";
export const HOVERED_CARD = "HOVERED_CARD";
export const UNHOVERED_CARD = "UNHOVERED_CARD";
