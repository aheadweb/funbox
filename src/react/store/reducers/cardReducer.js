import {FETCH_CARDS, HOVERED_CARD, SELECT_CARD, UNHOVERED_CARD, UNSELECT_CARD} from "../consts";

const intialState = {
    cards: [],
    selectCards: [],
    hoveredCards: []

};

const cardReducer = (state = intialState, action) => {
    switch (action.type) {
    case FETCH_CARDS: return {...state, cards: [...state.cards, ...action.payload]};
    case SELECT_CARD: return {...state, selectCards: [...state.selectCards, action.payload]};
    case UNSELECT_CARD: return {...state, selectCards: state.selectCards.filter(id => id !== action.payload)};
    case HOVERED_CARD: return {...state, hoveredCards: [...state.hoveredCards, action.payload]};
    case UNHOVERED_CARD: return {...state, hoveredCards: state.hoveredCards.filter(id => id !== action.payload)};
    default: return state;
    }
};

export default cardReducer;
