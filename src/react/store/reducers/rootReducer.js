import {combineReducers} from "redux";
import cardReducer from "./cardReducer";
import appReducer from "./appReducer";

const rootReducer = combineReducers({
    cards: cardReducer,
    app: appReducer
});

export default rootReducer;
