import {HIDE_LOADER, SHOW_LOADER} from "../consts";

const initialState = {
    loader: false,
};

const appReducer = (state = initialState, action) => {
    switch (action.type) {
    case SHOW_LOADER: return {...state, loader: true};
    case HIDE_LOADER: return {...state, loader: false};
    default: return state;
    }
};

export default appReducer;
