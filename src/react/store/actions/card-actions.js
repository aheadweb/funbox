import {FETCH_CARDS, HOVERED_CARD, SELECT_CARD, UNHOVERED_CARD, UNSELECT_CARD} from "../consts";
import {getCardsProduct} from "../../api";
import {hideLoader, showLoader} from "./app-actions";

function fetchCards() {
    return dispatch => {
        dispatch(showLoader());
        getCardsProduct()
            .then((result) => {
                if (result.success) {
                    dispatch({type: FETCH_CARDS, payload: result.data});
                    dispatch(hideLoader());
                }
            });
    };
}

function selectCard(id) {
    return {
        type: SELECT_CARD,
        payload: id
    };
}

function unSelectCard(id) {
    return {
        type: UNSELECT_CARD,
        payload: id,
    };
}

function hoveredCard(id) {
    return {
        type: HOVERED_CARD,
        payload: id
    };
}

function unHoveredCard(id) {
    return {
        type: UNHOVERED_CARD,
        payload: id,
    };
}

export {
    fetchCards,
    selectCard,
    unSelectCard,
    hoveredCard,
    unHoveredCard
};
