import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {hoveredCard, selectCard, unHoveredCard, unSelectCard} from "../../store/actions/card-actions";

const Card = (props) => {

    const dispatch = useDispatch();

    const {className, data} = props;
    const {
        title, subtitle, promo, promoHovered, cardDescHovered,
        inStock, weight, weightText, id, icon, description
    } = data;


    const isCheck = (useSelector(state => state.cards.selectCards)).includes(id);
    const [isLeave, setLeave] = useState(false);

    const isDisabled = !inStock ? "card_disabled" : "";
    const classChecked = isCheck ? "card_select" : "";
    const classLeave = isLeave ? "card_leave" : "";


    const toggleCard = (id) => {
        const dispatchSelect = !isCheck
            ? selectCard
            : unSelectCard;
        dispatch(dispatchSelect(id));
        if(!isCheck) setLeave(false);
    };


    const onHover = (id) => {
        if (!isCheck && !isLeave) {
            setLeave(true);
            dispatch(hoveredCard(id));
            setTimeout(() => {
                setLeave(false);
                dispatch(unHoveredCard(id));
            }, 3000);
        }
    };

    let promoText = !isLeave ? promo : promoHovered;

    const descriptionText = description.map((text, i) => {
        return (<div className="card__descr" key={i}>{text}</div>);
    });

    return (<div
        className={`${className === undefined ? "" : className} card ${classChecked} ${classLeave} ${isDisabled}`}
        onClick={() => toggleCard(id)}
        onMouseLeave={() => onHover(id)}>
        <div className="card__content">
            <div className="card__promo-text">{promoText}</div>
            <div className="card__title">{title}</div>
            <div className="card__subtitle">{subtitle}</div>
            {descriptionText}
            <div className="card__img">
                <img src={icon} alt="card-icon"/>
            </div>
            <div className="card__weight">
                <span className="card__weight-number">{weight}</span>
                <span className="card__weight-text">{weightText}</span>
            </div>
        </div>
    </div>);
};

export default Card;
