import React, {Component} from "react";
import Card from "../../components/card/index.jsx";
import {connect} from "react-redux";
import {fetchCards, selectCard} from "../../store/actions/card-actions";

class CardList extends Component {

    componentDidMount() {
        this.props.fetchCards();
    }

    render() {
        const isLoad = this.props.loading;
        const title = !isLoad ? "Ты сегодня покормил кота?" : "Загрузка данных...";

        const CardInRow = this.props.cards.map((card) => {

            const CardInStock = () => {
                return <React.Fragment>
                    Чего сидишь? Порадуй котэ,&nbsp;
                    <span onClick={() => this.props.selectCardDispatch(card.id)} className="card-list__item-link">
                        <span>купи</span>
                    </span>
                </React.Fragment>;
            };

            let cardContent = <CardInStock/>;

            this.props.selectCard.includes(card.id) || this.props.hoveredCard.includes(card.id)
                ? cardContent = card.cardDescHovered
                : null;


            const selectClassName = this.props.selectCard.includes(card.id) ? "card_select" : "";
            const inStockClassName = !card.inStock  ? "card-list__item_disabled" : "";
            const CardNotStock = `Печалька, ${card.subtitle} закончился.`;

            return <div className={`card-list__item ${inStockClassName}`} key={card.id}>
                <Card className={`card_in-row card-list__item-card ${selectClassName}`} data={card}/>
                <div className="card-list__item-descr">
                    {card.inStock
                        ? cardContent
                        : CardNotStock
                    }
                </div>
            </div>;
        });

        return <React.Fragment>
            <div className="card-list">
                <div className="container">
                    <div className="card-list__container">
                        <div className="card-list__title">{title}</div>
                        <div className="card-list__list">
                            {CardInRow}
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>;
    }
}

const mapStateToProps = (state) => {
    return {
        cards: state.cards.cards,
        selectCard: state.cards.selectCards,
        hoveredCard: state.cards.hoveredCards,
        loading: state.app.loader
    };
};

const mapDispatchToProps = {
    fetchCards,
    selectCardDispatch: selectCard
};

export default connect(mapStateToProps, mapDispatchToProps)(CardList);
